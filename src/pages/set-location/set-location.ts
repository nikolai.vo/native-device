import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

import { Location } from '../../models/location';

@Component({
  selector: 'page-set-location',
  templateUrl: 'set-location.html',
})
export class SetLocationPage {
  public location: Location;
  public marker: Location;


  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController) {

    this.location = this.navParams.get('location');
    if (this.navParams.get('isSet')) {
      this.marker = this.location;
    }

  }


  public onSetMarker(event: any) {
    console.log(event);
    this.marker = new Location(event.coords.lat, event.coords.lng);
  }

  public onConfirm() {
    this.viewCtrl.dismiss({ location: this.marker });
  }

  public onAbort() {
    this.viewCtrl.dismiss();
  }
}
