import { Component, OnInit } from '@angular/core';
import { ModalController } from 'ionic-angular';

import { AddPlacePage } from '../add-place/add-place';
import { Place } from '../../models/plase';
import { PlacesService } from '../../services/places.services';
import { PlasePage } from '../plase/plase';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  public addPlacePage = AddPlacePage;
  public places: Place[] = [];


  constructor(
    public modalCtrl: ModalController,
    private placesService: PlacesService) { }

  ngOnInit() {
    this.placesService.fetchPlaces()
      .then(
        (places: Place[]) => this.places = places
      );
  }

  ionViewWillEnter() {
    this.places = this.placesService.loadPlaces();
  }

  public onOpenPlace(place: Place, index: number) {
    const modal = this.modalCtrl.create(PlasePage, { place: place, index: index });
    modal.present();
    modal.onDidDismiss(
      () => {
        this.places = this.placesService.loadPlaces();
      }
    );
  }
}
