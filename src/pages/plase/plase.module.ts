import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlasePage } from './plase';

@NgModule({
  declarations: [
    PlasePage,
  ],
  imports: [
    IonicPageModule.forChild(PlasePage),
  ],
})
export class PlasePageModule {}
