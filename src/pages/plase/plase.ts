import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

import { Place } from '../../models/plase';
import { PlacesService } from '../../services/places.services';



@Component({
  selector: 'page-plase',
  templateUrl: 'plase.html',
})
export class PlasePage {

  public place: Place;
  public index: number;

  constructor(
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private placesService: PlacesService) {

    this.place = this.navParams.get('place');
    this.index = this.navParams.get('index')
  }

  public onLeave() {
    this.viewCtrl.dismiss();
  }

  public onDelete() {
    this.placesService.deletePlace(this.index);
    this.onLeave();
  }

}
