import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AgmCoreModule } from '@agm/core';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';

import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddPlacePage } from '../pages/add-place/add-place';
import { PlasePage } from '../pages/plase/plase';
import { SetLocationPage } from '../pages/set-location/set-location';
import { PlacesService } from '../services/places.services';
import { WebView } from '@ionic-native/ionic-webview/ngx';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddPlacePage,
    PlasePage,
    SetLocationPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDie8a00SuYnJhU9mc1nYZATCfMpJkD6sE'
    }),
    IonicStorageModule.forRoot()
    // AgmCoreModule.forRoot({
    //   apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    // })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddPlacePage,
    PlasePage,
    SetLocationPage
  ],
  providers: [
    Geolocation,
    Camera,
    File,
    StatusBar,
    SplashScreen,
    PlacesService,
    WebView,
    { provide: ErrorHandler, useClass: IonicErrorHandler },

  ]
})
export class AppModule { }
